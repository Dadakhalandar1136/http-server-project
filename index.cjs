const http = require('http');
const fs = require('fs');
const uuid = require('uuid');
const PORT = process.env.PORT || 3030;


const server = http.createServer((req, resp) => {

    const statusNumber = Number((req.url).split('/').slice(-1));
    const delay_in_seconds = statusNumber;

    if (req.url === '/html' && req.method === "GET") {
        fs.readFile('./index.html', "utf-8", (error, data) => {
            if (error) {
                console.error(error);
                resp.writeHead(500, { 'Content-type': 'application/json' });
                const errorMessage = {
                    message: 'Internal server error',
                    error: 'File path is invalid or file name is invalid'
                }
                resp.write(JSON.stringify(errorMessage));
                resp.end();
            } else {
                resp.writeHead(200, { 'Content-Type': 'text/html' });
                resp.write(data);
                resp.end();
            }
        });

    } else if (req.url === '/json' && req.method === "GET") {
        fs.readFile('./dat.json', (error, data) => {
            if (error) {
                console.error(error);
                resp.writeHead(500, { "Content-type": "application/json" });
                const errorMessage = {
                    message: "Internal server error",
                    error: "File path is invalid or file name is invalid"
                }
                resp.write(JSON.stringify(errorMessage));
                resp.end();
            } else {
                resp.writeHead(200, { 'Content-Type': 'application/json' });
                resp.write(data);
                resp.end();
            }
        });

    } else if (req.url === '/uuid' && req.method === "GET") {
        const uuidStore = uuid.v4();
        const uuidObject = {
            uuid: uuidStore
        }
        resp.writeHead(200, { 'Content-Type': 'application/json' });
        resp.write(JSON.stringify(uuidObject));
        resp.end();

    } else if (req.url === `/status/${statusNumber}` && req.method === "GET") {
        const statusCode = http.STATUS_CODES[statusNumber];
        if (statusCode === undefined) {
            resp.writeHead(500, { "Content-type": "application/json" });
            const errorMessage = {
                message: "Internal server error",
                error: "Entered status code is invalid"
            }
            resp.write(JSON.stringify(errorMessage));
            resp.end();
        } else {
            resp.writeHead(statusNumber, { 'Content-type': 'text/plain' });
            resp.write(statusCode);
            resp.end();
        }

    } else if (req.url === `/delay/${delay_in_seconds}` && req.method === "GET") {
        if (delay_in_seconds < 0) {
            resp.writeHead(500, { "Content-type": "application/json" });
            const errorMessage = {
                message: "Internal server error",
                error: "cannot accept negative value or the string"
            }
            resp.write(JSON.stringify(errorMessage));
            resp.end();
        } else {
            setTimeout(() => {
                resp.writeHead(200);
                resp.write("delay seconds :" + delay_in_seconds);
                resp.end();
            }, delay_in_seconds * 1000);
        }

    } else {
        resp.writeHead(500);
        resp.write("Invalid request");
        resp.end();
    }
});


server.listen(PORT, () => {
    console.log(`server started on port ${PORT}`);
});


